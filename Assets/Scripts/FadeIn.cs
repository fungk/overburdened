﻿using UnityEngine;
using System.Collections;

public class FadeIn : MonoBehaviour {

	public Color startColor;
	public Color endColor;

	public float fadeTime;

	float timer;

	SpriteRenderer ren;

	// Use this for initialization
	void Start () {
		ren = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		ren.material.color = Color.Lerp (startColor, endColor, timer/fadeTime);
	}
}
