﻿using System;
using UnityEngine;
using System.Collections;

public class AgentParameters : MonoBehaviour {

	public GameObject model;
	
	public event EventHandler OnDie;

	public bool leader;

	public Color startColor;
	public Color finalColor;

	public float maxLifespan;
	public float minLifespan;

	float externalColorPercentage;
	float colorPercentage;
	float lastColorPercentage;

	float lifespan;
	float lifeTimer;

	Renderer ren;

	DeathHandler death;

	BirdWatcher watcher;

	// Use this for initialization
	void Start () {
		lastColorPercentage = 999f;
		ren = model.renderer;

		lifespan = UnityEngine.Random.Range(minLifespan, maxLifespan);

		death = GameObject.FindWithTag ("Manager").GetComponent<DeathHandler>();
		watcher = death.GetComponent<BirdWatcher>();
	}
	
	// Update is called once per frame
	void Update () {
		lifeTimer += Time.deltaTime;
		CalculateColorPercentage();
		if(!leader && lifeTimer >= lifespan){
			watcher.StarveBird (gameObject);
		}
		if(lastColorPercentage != colorPercentage){
			lastColorPercentage = colorPercentage;
			ren.material.color = Color.Lerp (startColor, finalColor, colorPercentage);
		}
	}

	public void KillBird(){
		if(OnDie != null){
			OnDie(gameObject, new EventArgs());
		}
		death.SpawnSound(transform.position);
		Destroy (gameObject);
	}

	public void SetColorPercentage(float percentage){
		externalColorPercentage = percentage;
	}

	void CalculateColorPercentage(){
		if(!leader){
			colorPercentage = Mathf.Max (externalColorPercentage, lifeTimer/lifespan);
		}
		else{
			colorPercentage = externalColorPercentage;
		}
	}
}
