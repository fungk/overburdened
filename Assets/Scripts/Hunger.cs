﻿using System;
using UnityEngine;
using System.Collections;

public class Hunger : MonoBehaviour {

	public float starveTime;

	public float initialBonusTime;

	public float maxStarveTime;

	GameObject starvingBird;

	float starveTimer;
	bool initialTime;

	BirdWatcher watcher;
	AgentParameters bird;

	// Use this for initialization
	void Start () {
		starveTimer = -initialBonusTime;
		initialTime = true;

		watcher = GameObject.FindWithTag("Manager").GetComponent<BirdWatcher>();
		watcher.OnSpawnBird += NewBird;

		FindNewStarvingBird ();
	}
	
	// Update is called once per frame
	void Update () {
		starveTimer += Time.deltaTime;
		if(starveTimer > -maxStarveTime + starveTime){
			initialTime = false;
		}
		if(starveTimer >= starveTime){
			AddTime ();
			StarveBird();
		}
		if(bird != null){
			bird.SetColorPercentage(Mathf.Max (0f, starveTimer/starveTime));
		}
	}

	void NewBird(object newBird, EventArgs e){
		AddTime ();
		if(starvingBird != null && starvingBird.GetComponent<AgentParameters>().leader){
			starvingBird.GetComponent<AgentParameters>().OnDie -= StarvingBirdDead;
			FindNewStarvingBird();
		}
	}

	void StarvingBirdDead(object bird, EventArgs e){
		if(((GameObject)bird).Equals (starvingBird)){
			FindNewStarvingBird();
		}
	}

	void AddTime(){
		if(!initialTime){
			starveTimer = Mathf.Max (-maxStarveTime + starveTime, starveTimer - starveTime);
		}
	}

	void FindNewStarvingBird(){
		if(bird != null){
			bird.SetColorPercentage(0f);
		}
		if(watcher.birds.Count > 0){
			starvingBird = watcher.birds[UnityEngine.Random.Range ((int)0, (int)watcher.birds.Count)];
			starvingBird.GetComponent<AgentParameters>().OnDie += StarvingBirdDead;
		}
		else if(watcher.leaders.Count > 0){
			starvingBird = watcher.leaders[0];
			starvingBird.GetComponent<AgentParameters>().OnDie += StarvingBirdDead;
		}
		else{
			starvingBird = null;
		}
		if(starvingBird != null){
			bird = starvingBird.GetComponent<AgentParameters>();
		}
	}

	void StarveBird(){
		if(starvingBird != null){
			watcher.StarveBird (starvingBird);
			FindNewStarvingBird();
		}
	}
}
