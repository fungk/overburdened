﻿using System;
using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

	public event EventHandler OnBirdCollide;
	public event EventHandler OnLeaderCollide;

	public float shiftDistance;
	public float shiftTime;
	public AnimationCurve shiftCurve;
	public float shiftCap;
	public float featherDistance;

	public enum WallEnum {Top, Bot, Left, Right};
	public WallEnum wallSide;

	Vector3 targetPos;
	Vector3 origPos;
	float shiftTimer = 999f;

	// Use this for initialization
	void Start () {
		targetPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		shiftTimer += Time.deltaTime;
		if(shiftTimer < shiftTime){
			transform.position = origPos + (targetPos-origPos)*shiftCurve.Evaluate (shiftTimer/shiftTime);
			//transform.position = Vector3.Lerp (origPos, targetPos, shiftTimer/shiftTime);
		}
	}

	void ShiftWall(float distance){
		//if(shiftTimer >= shiftTime){
			shiftTimer = 0f;
		//}

		origPos = transform.position;
		targetPos += VectorFromSide()*distance;

		if((targetPos - origPos).magnitude > shiftCap){
			targetPos = origPos + VectorFromSide() * shiftCap;
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		float shiftAmount = 0f;
		if(col.tag == "Leader" && OnLeaderCollide != null){
			shiftAmount = shiftDistance;
			OnLeaderCollide(col.gameObject, new EventArgs());
		}
		if(col.tag == "Bird" && OnBirdCollide != null){
			shiftAmount = shiftDistance;
			OnBirdCollide(col.gameObject, new EventArgs());
		}
		if(col.tag == "Feather"){
			shiftAmount = featherDistance; 
			Destroy (col.gameObject);
		}
		ShiftWall(shiftAmount);
	}

	Vector3 VectorFromSide(){
		switch(wallSide){
		case WallEnum.Bot:
			return Vector3.up;
		case WallEnum.Left:
			return Vector3.right;
		case WallEnum.Top:
			return Vector3.down;
		default:
			return Vector3.left;
		}
	}
}
