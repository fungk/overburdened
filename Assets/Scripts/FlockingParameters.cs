﻿using UnityEngine;
using System.Collections;
using System;

public class FlockingParameters : MonoBehaviour{

	public float speed;
	public float visionAngle;
	public float alignmentDistance;
	public float cohesionDistance;
	public float separationDistance;

	public float separationDistanceLarge;
	public float separationWeightLarge;

	public float alignmentWeight;
	public float cohesionWeight;
	public float separationWeight;
	
	public float visionAngleLead;
	public float alignmentDistanceLead;
	public float cohesionDistanceLead;
	public float separationDistanceLead;
	
	public float alignmentWeightLead;
	public float cohesionWeightLead;
	public float separationWeightLead;

	public float scatterDistance;
	public float scatterSeparationWeight;

	void Start(){
		GetComponent<BirdWatcher>().OnGameOver += ScatterBirds;
	}

	void ScatterBirds(object sender, EventArgs e){
		separationWeight = scatterSeparationWeight;
		separationDistance = scatterDistance;
	}
}
