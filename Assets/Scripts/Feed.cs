﻿using System;
using UnityEngine;
using System.Collections;

public class Feed : MonoBehaviour {

	public event EventHandler OnFeed;

	public float duration;
	public float fadeDelay;

	public Color startColor;
	public Color endColor;

	SpriteRenderer ren;

	float timer;

	// Use this for initialization
	void Start () {
		ren = GetComponent<SpriteRenderer>();
		GameObject go = GameObject.FindWithTag ("Leader");
		if(go != null){
			go.GetComponent<AgentParameters>().OnDie += Remove;
		}
		else{
			Destroy (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if(timer > fadeDelay){
			ren.material.color = Color.Lerp (startColor, endColor, (timer-fadeDelay)/(duration-fadeDelay));
		}
		if(timer > duration){
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "Bird" || col.gameObject.tag == "Leader"){
			if(OnFeed != null){
				OnFeed(gameObject, new EventArgs());
			}
			Destroy (gameObject);
		}
	}

	void Remove(object sender, EventArgs e){
		try{
			Destroy (gameObject);
		}
		catch{};
	}
}
