﻿using UnityEngine;
using System.Collections;

public class SoundDeleter : MonoBehaviour {

	public AudioClip[] sounds;

	AudioSource audioSource;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(gameObject);
		audioSource = GetComponent<AudioSource>();

		audioSource.clip = sounds[Random.Range ((int)0, (int)sounds.Length)];
		audioSource.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		if(!audioSource.isPlaying){
			Destroy (gameObject);
		}
	}
}
