﻿using UnityEngine;
using System.Collections;

public class LeaderBehavior : MonoBehaviour {

	public float speed;
	public float minDistance;
	public float slowDistance;

	public float deadZone;

	Vector2 target;
	float speedPercentage;

	bool gamepad;

	Vector2 oldMouse;
	Vector2 oldGamepad;

	// Use this for initialization
	void Start () {
		oldMouse = Input.mousePosition;
		oldGamepad = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis ("Vertical"));
		gamepad = false;
	}
	
	// Update is called once per frame
	void Update () {

		Vector2 axes = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis ("Vertical"));

		if((oldGamepad - axes).magnitude > 0.1f){
			gamepad = true;
		}
		else if((oldMouse - (Vector2)Input.mousePosition).magnitude > 0.1f){
			gamepad = false;
		}

		oldGamepad = axes;
		oldMouse = Input.mousePosition;

		if(gamepad){
			target = GetGamepadTarget(axes);
			Screen.showCursor = false;
		}
		else{
			target = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Screen.showCursor = true;
		}

		Vector2 targetVec = (target - (Vector2)transform.position);
		float angle = Vector2.Angle (Vector2.right, targetVec);
		if(Vector2.Angle (Vector2.up, targetVec) > 90){
			angle *= -1;
		}
		transform.rotation = Quaternion.Euler (new Vector3(0,0,angle));

		if(targetVec.magnitude > slowDistance){
			speedPercentage = 1.0f;
		}
		else if(targetVec.magnitude > minDistance){
			speedPercentage = (targetVec.magnitude - minDistance) / (slowDistance - minDistance);
		}
		else{
			speedPercentage = 0f;
		}
	}

	Vector2 GetGamepadTarget(Vector2 axes){
		Vector2 pos = transform.position;
		if(axes.magnitude > deadZone){
			pos += axes.normalized * 2f;
		}
		else{
			pos += (Vector2)transform.right * 2f;
		}
		return pos;
	}

	void FixedUpdate()
	{
		rigidbody2D.velocity = transform.right * speed * speedPercentage;
	}
}
