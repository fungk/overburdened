﻿using System;
using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

	public float fadeOutTime;
	public float sceneDelay;

	public Color startColor;
	public Color endColor;

	float timer;
	bool gameOver = false;
	bool fadedOut = false;

	AudioSource music;

	// Use this for initialization
	void Start () {
		music = GameObject.FindWithTag ("Music").GetComponent<AudioSource>();
		GameObject.FindWithTag("Leader").GetComponent<AgentParameters>().OnDie += EndGame;
	}
	
	// Update is called once per frame
	void Update () {
		if(gameOver){
			timer += Time.deltaTime;
			if(!fadedOut){
				Camera.main.backgroundColor = Color.Lerp (startColor, endColor, timer/fadeOutTime);
				music.volume = Mathf.Max (0f, 1-(timer/fadeOutTime));
				if(timer >= fadeOutTime){
					fadedOut = true;
					timer = 0f;
				}
			}
			else{
				if(timer >= sceneDelay){
					Application.LoadLevel ("Title");
				}
			}
		}
	}

	public void EndGame(object sender, EventArgs e){
		gameOver = true;
	}
}
