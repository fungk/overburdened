﻿using UnityEngine;
using System.Collections;

public class TitleAnimation : MonoBehaviour {

	public float movieDelay;
	public float movieTime;
	float timer;

	bool delay;

	// Use this for initialization
	void Start () {
		delay = true;
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if(delay && timer > movieDelay){
			timer = 0f;
			((MovieTexture)renderer.material.mainTexture).Play();
			delay = false;
		}
		if(!delay && timer > movieTime){
			((MovieTexture)renderer.material.mainTexture).Pause ();
		}
	}
}
