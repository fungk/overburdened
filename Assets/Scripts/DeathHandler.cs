﻿using UnityEngine;
using System.Collections;

public class DeathHandler : MonoBehaviour {

	public GameObject deathSound;

	public float deathTimeout;

	float deathTimer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		deathTimer += Time.deltaTime;
	}

	public void SpawnSound(Vector3 pos){
		if(deathTimer >= deathTimeout){
			deathTimer = 0f;
			Instantiate (deathSound, pos, Quaternion.identity);
		}
	}
}
