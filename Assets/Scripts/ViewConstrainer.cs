﻿using UnityEngine;
using System.Collections;

public class ViewConstrainer : MonoBehaviour {

	//public GameObject black;

	public float horizontalPlaySpace;
	public float verticalPlaySpace;
	public float wallBuffer;
	public float wallZ;
	//public float blackZ;

	// Use this for initialization
	void Start () {
		GameObject[] walls = GameObject.FindGameObjectsWithTag("Wall");
		Vector3 scale = new Vector3(horizontalPlaySpace*1.5f, verticalPlaySpace*1.5f, 1f);
		//GameObject go;
		foreach(GameObject wall in walls){
			wall.transform.localScale = scale;
			switch(wall.GetComponent<Wall>().wallSide){
			case Wall.WallEnum.Left:
				wall.transform.position = new Vector3(-((horizontalPlaySpace + scale.x)/2f + wallBuffer), 0, wallZ);
				//go = (GameObject)Instantiate (black, new Vector3(-(horizontalPlaySpace + scale.x)/2f, 0, blackZ), Quaternion.identity);
				//go.transform.localScale = scale;
				break;
			case Wall.WallEnum.Right:
				wall.transform.position = new Vector3((horizontalPlaySpace + scale.x)/2f + wallBuffer, 0, wallZ);
				//go = (GameObject)Instantiate (black, new Vector3((horizontalPlaySpace + scale.x)/2f, 0, blackZ), Quaternion.identity);
				//go.transform.localScale = scale;
				break;
			case Wall.WallEnum.Bot:
				wall.transform.position = new Vector3(0, -((verticalPlaySpace + scale.y)/2f + wallBuffer), wallZ);
				//go = (GameObject)Instantiate (black, new Vector3(0, -(verticalPlaySpace + scale.y)/2f, blackZ), Quaternion.identity);
				//go.transform.localScale = scale;
				break;
			default:
				wall.transform.position = new Vector3(0, (verticalPlaySpace + scale.y)/2f + wallBuffer, wallZ);
				//go = (GameObject)Instantiate (black, new Vector3(0, (verticalPlaySpace + scale.y)/2f, blackZ), Quaternion.identity);
				//go.transform.localScale = scale;
				break;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
