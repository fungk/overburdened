﻿using UnityEngine;
using System.Collections;

public class BirdFeeder : MonoBehaviour {

	public GameObject feed;
	public float safeDistance;
	public float zPos;

	public float feedTime;

	public int entropy1;
	public int entropy2;
	public int entropy3;
	public int entropy4;
	public int entropy5;

	float feedTimer;

	GameObject[] walls;

	BirdWatcher watcher;

	// Use this for initialization
	void Start () {
		watcher = GetComponent<BirdWatcher>();
		walls = GameObject.FindGameObjectsWithTag("Wall");
	}
	
	// Update is called once per frame
	void Update () {
		feedTimer += Time.deltaTime;
		if(feedTimer > feedTime){
			SpawnFeed ();
			feedTimer = 0f;
		}
	}

	void SpawnFeed(){
		GameObject go = (GameObject)Instantiate (feed, GenerateFeedPosition(), Quaternion.identity);
		go.GetComponent<Feed>().OnFeed += watcher.SpawnBird;
		go.GetComponent<Animator>().Play ("flower" + GetEntropy() + "-spawn");
	}

	Vector3 GenerateFeedPosition(){
		float top = 0f, bot = 0f, left = 0f, right = 0f;
		foreach(GameObject wall in walls){
			switch(wall.GetComponent<Wall>().wallSide){
			case Wall.WallEnum.Top:
				top = wall.transform.position.y - wall.transform.localScale.y/2.0f;
				break;
			case Wall.WallEnum.Bot:
				bot = wall.transform.position.y + wall.transform.localScale.y/2.0f;
				break;
			case Wall.WallEnum.Left:
				left = wall.transform.position.x + wall.transform.localScale.x/2.0f;
				break;
			default:
				right = wall.transform.position.x - wall.transform.localScale.x/2.0f;
				break;
			}
		}
		float xSafety = safeDistance;
		if(right - left <= safeDistance*2){
			xSafety = 0f;
		}
		float ySafety = safeDistance;
		if(top-bot <= safeDistance *2){
			ySafety = 0f;
		}

		top -= ySafety;
		bot += ySafety;
		right -= xSafety;
		left += xSafety;

		return new Vector3(Random.Range (left, right), Random.Range (bot, top), zPos);
	}

	int GetEntropy(){
		if(watcher.entropy < entropy1){
			return 1;
		}
		else if(watcher.entropy < entropy2){
			return 2;
		}
		else if(watcher.entropy < entropy3){
			return 3;
		}
		else if(watcher.entropy < entropy4){
			return 4;
		}
		else if(watcher.entropy < entropy5){
			return 5;
		}
		else{
			return 6;
		}
	}
}
