﻿using UnityEngine;
using System.Collections;

public class BirdAnimator : MonoBehaviour {

	public float flapTime;

	public float minFlap;
	public float maxFlap;

	Animator animator;

	float sumTime;
	float flapTimer;
	bool flapping;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		sumTime = flapTime + Random.Range (minFlap, maxFlap);
		flapping = true;
	}
	
	// Update is called once per frame
	void Update () {
		flapTimer += Time.deltaTime;
		if(flapping && flapTimer >= flapTime){
			flapping = false;
			animator.Play ("gliding");
		}
		if(flapTimer >= sumTime){
			sumTime = flapTime + Random.Range (minFlap, maxFlap);
			flapTimer = 0f;
			flapping = true;
			Flap ();
		}
	}

	public void Flap(){
		flapping = true;
		animator.Play ("flying");
	}
}
