﻿using UnityEngine;
using System.Collections;

public class GamePadControl : MonoBehaviour {
	
	public float speed;
	public float minDistance;
	public float slowDistance;
	
	Vector2 target;
	float speedPercentage;
	
	// check if user has a gamepad
	public bool gamePadPresent;
	public Vector3 movedirection;
	
	// Use this for initialization
	void Start () {
		if (Input.GetJoystickNames ().Length == 0)
			gamePadPresent = false;
		else
			gamePadPresent = true;
		print (gamePadPresent);
	}
	
	// Update is called once per frame
	void Update () {
		if (!gamePadPresent) {
			target = (Vector2)Camera.main.ScreenToWorldPoint (Input.mousePosition);
			Vector2 targetVec = (target - (Vector2)transform.position);
			float angle = Vector2.Angle (Vector2.right, targetVec);
			if (Vector2.Angle (Vector2.up, targetVec) > 90) {
				angle *= -1;
			}
			transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angle));
			
			if (targetVec.magnitude > slowDistance) {
				speedPercentage = 1.0f;
			} else if (targetVec.magnitude > minDistance) {
				speedPercentage = (targetVec.magnitude - minDistance) / (slowDistance - minDistance);
			} else {
				speedPercentage = 0f;
			}
		}
		else {
			// need to cast horPos, verPos, newRotation
			float horPos; float verPos;  Quaternion newRotation; float smooth;smooth =1f;

			horPos = Input.GetAxis("Horizontal");
			verPos = Input.GetAxis("Vertical");
			
			movedirection = new Vector3(horPos, verPos,0);
			
			if(movedirection != Vector3.zero){
				print(movedirection);
				//newRotation = Quaternion.LookRotation(movedirection*-1);
				//transform.rotation = Quaternion.Slerp(transform.rotation,newRotation,Time.deltaTime*smooth);
			}
			
		}
	}
	
	void FixedUpdate()
	{
		if (!gamePadPresent) {
			rigidbody2D.velocity = transform.right * speed * speedPercentage;
		}
		else{
			//rigidbody2D.AddForce(movedirection*speed*Time.deltaTime);
			if(rigidbody2D.velocity!=Vector2.zero){
				float angle = Vector2.Angle (Vector2.right, rigidbody2D.velocity);
				if(Vector2.Angle (Vector2.up, rigidbody2D.velocity) > 90){
					angle *= -1;
				}
				transform.rotation = Quaternion.Euler (new Vector3(0,0,angle));
			}
			rigidbody2D.velocity = movedirection*speed*Time.deltaTime;
		}
	}
}
