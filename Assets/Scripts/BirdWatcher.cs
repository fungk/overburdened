﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BirdWatcher : MonoBehaviour {

	public event EventHandler OnGameOver;

	public event EventHandler OnSpawnBird;

	public int featherCount;
	public float featherForce;

	public GameObject bird;
	public GameObject feather;
	public float maxSpawnAngle;

	public List<GameObject> birds = new List<GameObject>();
	public List<GameObject> leaders = new List<GameObject>();

	public Queue<GameObject> flockingQueue = new Queue<GameObject>();

	public int entropy;

	// Use this for initialization
	void Awake () {
		GameObject[] gos = GameObject.FindGameObjectsWithTag("Bird");
		foreach(GameObject go in gos)
		{
			birds.Add(go);
		}
		GameObject[] leads = GameObject.FindGameObjectsWithTag("Leader");
		foreach(GameObject lead in leads)
		{
			leaders.Add (lead);
		}

		GameObject[] walls = GameObject.FindGameObjectsWithTag("Wall");
		foreach(GameObject wall in walls)
		{
			wall.GetComponent<Wall>().OnBirdCollide += RemoveBird;
			wall.GetComponent<Wall>().OnLeaderCollide += RemoveLeader;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown (KeyCode.Escape)){
			Application.Quit ();
		}
		FlockBirds();
	}

	public void FlockBirds(){
		for(int i = 0; i < Mathf.Pow (flockingQueue.Count, 2/(float)Math.E); i++){
			GameObject go = flockingQueue.Dequeue();
			if(go != null){
				go.GetComponent<FlockingAgent>().Flock ();
				flockingQueue.Enqueue(go);
			}
		}
	}

	public void SpawnBird(object sender, EventArgs e){
		entropy++;
		Vector2 position = ((GameObject)sender).transform.position;
		GameObject newBird = (GameObject)Instantiate (bird, position, Quaternion.identity);
		if(leaders.Count > 0){
			newBird.transform.eulerAngles = new Vector3(0,0, leaders[0].transform.rotation.eulerAngles.z + UnityEngine.Random.Range (-maxSpawnAngle, maxSpawnAngle) % 360);
		}

		birds.Add(newBird);
		flockingQueue.Enqueue (newBird);

		if(OnSpawnBird != null){
			OnSpawnBird(newBird, new EventArgs());
		}
	}

	public void RemoveBird(object bird, EventArgs e){
		entropy++;
		birds.Remove ((GameObject)bird);
		((GameObject)bird).GetComponent<AgentParameters>().KillBird();
	}

	public void RemoveLeader(object leader, EventArgs e){
		leaders.Remove ((GameObject)leader);
		((GameObject)leader).GetComponent<AgentParameters>().KillBird();
		GameOver ();
	}

	public void StarveBird(GameObject bird){
		entropy++;
		if(bird.GetComponent<AgentParameters>().leader){
			leaders.Remove (bird);
		}
		else{
			birds.Remove (bird);
		}
		for(int i = 0; i < featherCount; i++){
			SpawnFeather (bird.transform.position);
		}
		((GameObject)bird).GetComponent<AgentParameters>().KillBird();
	}

	void SpawnFeather(Vector3 pos){
		GameObject go = (GameObject)Instantiate (feather, pos, Quaternion.identity);
		go.transform.eulerAngles = new Vector3(0f, 0f, UnityEngine.Random.Range (0f, 360f));
		go.rigidbody2D.AddForce (go.transform.right * featherForce);
	}

	void GameOver(){
		if(OnGameOver != null){
			OnGameOver(gameObject, new EventArgs());
		}
	}
}
