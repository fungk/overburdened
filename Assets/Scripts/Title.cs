﻿using UnityEngine;
using System.Collections;

public class Title : MonoBehaviour {

	public GameObject plane;

	public Color startColor;
	public Color endColor;

	public float colorTime;

	float timer;

	bool silent = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Screen.showCursor = false;
		if(Input.GetKeyDown (KeyCode.Escape)){
			Application.Quit ();
		}
		if(Input.anyKeyDown){
			Application.LoadLevel ("Main");
		}
		timer += Time.deltaTime;
		if(plane != null){
			plane.renderer.material.color = Color.Lerp (startColor, endColor, timer/colorTime);
		}
		if(silent && timer >= colorTime){
			silent = false;
			GetComponent<AudioSource>().Play ();
		}
		if(plane != null && timer >= colorTime+0.1f){
			Destroy (plane);
		}
	}
}
